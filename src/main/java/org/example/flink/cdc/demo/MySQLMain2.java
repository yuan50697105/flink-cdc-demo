package org.example.flink.cdc.demo;

import lombok.SneakyThrows;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class MySQLMain2 {
    @SneakyThrows
    public static void main(String[] args) {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamTableEnvironment = getStreamTableEnvironment(environment);
        final Table table = getTable(streamTableEnvironment);
        TableResult tableResult = table.execute();
        tableResult.print();
    }

    private static Table getTable(StreamTableEnvironment streamTableEnvironment) {
        return streamTableEnvironment.sqlQuery(
                "select " +
                        "id," +
                        "name" +
                        " from users ");
    }

    private static StreamTableEnvironment getStreamTableEnvironment(StreamExecutionEnvironment environment) {
        StreamTableEnvironment streamTableEnvironment = StreamTableEnvironment.create(environment);
        streamTableEnvironment.executeSql("CREATE TABLE users (" +
                "    id INT," +
                "    name STRING," +
                "    PRIMARY KEY (id) NOT ENFORCED " +
                "  ) WITH (" +
                "    'connector' = 'mysql-cdc'," +
                "    'hostname' = '8.140.177.12'," +
                "    'port' = '3306'," +
                "    'username' = 'root'," +
                "    'password' = 'root'," +
                "    'database-name' = 'demo'," +
                "    'table-name' = 'users'" +
                "  )");
        return streamTableEnvironment;
    }
}
